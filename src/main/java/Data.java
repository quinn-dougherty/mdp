import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Data {

    public Connection connection;

    /**
     * Creates data.
     * @param numStates
     * @param numActions
     */
    public void createData(Integer numStates, Integer numActions) throws SQLException {
        Connection conn;
        conn = connect();

        //create tables if they don't exist, drop first if they do exist.
        createMachineTable(conn);
        createRewardsTable(conn);

        List<String> machine;
        List<String> rewards;
        machine = generateMachineData(numStates, numActions);
        rewards = generateRewardsData(machine);

        writeMachineData(conn, machine);
        writeRewardsData(conn, rewards);

        conn.close();
    }

    /**
     * Connect to the postgresql database.
     * @return java.sql.Connection object
     */
    public Connection connect() throws SQLException {

        final String url = "jdbc:postgresql://0.0.0.0:5432/mdp";
        final String user = "postgres";
        final String password = "password";

        Connection conn;
        conn = DriverManager.getConnection(url, user, password);
        //System.out.println("Connected to database server successfully");
        return conn;
    }


    /** Creates table machine.
     * @param conn
     */
    private void createMachineTable(Connection conn) throws SQLException {
        Statement statement;
        String create;
        create = "DROP TABLE IF EXISTS machine; " +
                "CREATE TABLE IF NOT EXISTS machine (" +
                "start_state TEXT NOT NULL, " +
                "action INT NOT NULL, " +
                "probability REAL NOT NULL, " +
                "end_state_heads TEXT NOT NULL, " +
                "end_state_tails TEXT NOT NULL, " +
                "PRIMARY KEY (start_state, action)" +
                ");";
        statement = conn.createStatement();
        statement.execute(create);
        statement.close();

    }

    /**
     * Creates table rewards.
     * @param conn
     */
    private void createRewardsTable(Connection conn) throws SQLException {
        Statement statement;
        String query;
        query = "DROP TABLE IF EXISTS rewards;" +
                "CREATE TABLE IF NOT EXISTS rewards (" +
                "state TEXT NOT NULL PRIMARY KEY, " +
                "reward INT NOT NULL" +
                ");";
        statement = conn.createStatement();
        statement.execute(query);
        statement.close();
    }

    /**
     * randomly generate machine.
     * @return The state transition function given in comma-separated table.
     */
    private List<String> generateMachineData(Integer numStates, Integer numActions) {
        MyRandom random;
        String machineHeader;
        String[] states;
        Integer[] actions;
        states = new String[numStates];
        actions = new Integer[numActions];
        random = new MyRandom();

        for (int i = 0; i < numStates; i++) {
            states[i] = "q" + String.valueOf(i);
        }
        for (int i = 0; i < numActions; i++) {
            actions[i] = i;
        }

        machineHeader = "start_state,action,probability,end_state_heads,end_state_tails";

        String machineRow;
        ArrayList<String> machineRows;
        machineRows = new ArrayList<String>();
        machineRows.add(machineHeader);

        System.out.print("Generating machine...");
        for (String state : states) {
            for (Integer action : actions) {
                ArrayList<String> machineRowList;
                machineRowList = new ArrayList<String>() {{
                    add(state);
                    add(String.valueOf(action));
                    add(String.valueOf(random.nextFloat()));
                    add(String.valueOf(states[random.nextInt(numStates)]));
                    add(String.valueOf(states[random.nextInt(numStates)]));
                }};
                machineRow = String.join(",", machineRowList);
                machineRows.add(machineRow);
            }
        }
        return machineRows;
    }


    /**
     * Generate rewards data.
     * @param machine
     * @return the reward data key value pair in "key,value" strings.
     */
    private List<String> generateRewardsData(List<String> machine) {
        MyRandom random;
        random = new MyRandom();
        String rewardsHeader;
        rewardsHeader = "state,reward";
        HashSet<String> states;
        states = new HashSet<String>() {};
        for (String machineRow : machine) {
            String[] row;
            row = machineRow.split(",");
            states.add(row[0]);
        }

        // i can't states not to add start_state.
        // If I remove the first row from machine it removes it globally.
        try { states.remove("start_state");
        } catch (NullPointerException e) { System.out.println("start_state isn't in there"); }

        ArrayList<String> rewardsRows;
        rewardsRows = new ArrayList<String>() {};
        rewardsRows.add(rewardsHeader);
        System.out.print("Generating rewards...");
        for (String state : states) {
            rewardsRows.add(String.format("%s,%s", state, String.valueOf(random.nextInt(11) - 5)));
        }

        return rewardsRows;
    }


    /**
     * Writes state transition data to table
     * @param conn
     * @param machine
     * @throws SQLException
     */
    private void writeMachineData(Connection conn, List<String> machine) throws SQLException {
        Statement statement;
        statement = conn.createStatement();
        String command;
        String[] rowArray;
        final String headers = machine.get(0);
        machine.remove(0);
        command = "INSERT INTO machine ( " + headers + ") VALUES ";
        for (String row : machine) {
            rowArray = row.split(",");
            command += String.format("('%s', %d, %f, '%s', '%s'),",
                    rowArray[0], Integer.valueOf(rowArray[1]), Float.valueOf(rowArray[2]), rowArray[3], rowArray[4]);
        }
        command = command.substring(0, command.length() - 1) + ";";
        System.out.print("Writing machine to table...");
        statement.execute(command);
        statement.close();
    }

    /**
     * Writes rewards data to table.
     * @param conn
     * @param rewards
     * @throws SQLException
     */
    private void writeRewardsData(Connection conn, List<String> rewards) throws SQLException {
        Statement statement;
        statement = conn.createStatement();
        String command;
        String[] rowArray;
        final String header = rewards.get(0);
        rewards.remove(0);
        command = String.format("INSERT INTO rewards (%s) VALUES ", header);
        for (String row : rewards) {
            rowArray = row.split(",");
            if (rowArray[0] != "start_state") {
                command += String.format("('%s',%d),", rowArray[0], Integer.valueOf(rowArray[1]));
            }
        }
        command = command.substring(0, command.length() - 1) + ";";
        System.out.print("Writing rewards to table...");
        statement.execute(command);
        statement.close();
    }

}
