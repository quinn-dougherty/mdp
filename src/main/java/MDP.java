import org.javatuples.Pair;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class MDP {
    public HashMap<Pair<String, Integer>, Supplier<String>> transitionFunction;
    public Map<String, Float> rewards;

    public MDP(Connection conn) throws SQLException {
        this.transitionFunction = readInTransitionFunction(conn);
        this.rewards = readInRewards(conn);
    }

    /**
     * Read in machine.
     * @param conn
     * @return Probabilistic state transition function.
     * @throws SQLException
     */
    private HashMap<Pair<String, Integer>, Supplier<String>> readInTransitionFunction(
            Connection conn) throws SQLException {

        MyRandom random;
        random = new MyRandom();
        HashMap<Pair<String, Integer>, Supplier<String>> transitionFunction;
        transitionFunction = new HashMap<Pair<String, Integer>, Supplier<String>>() {};

        Statement statement;
        statement = conn.createStatement();
        ResultSet machineResult;
        machineResult = statement.executeQuery(
                "SELECT start_state,action,probability,end_state_heads,end_state_tails FROM machine;"
        );

        String startState;
        Integer action;
        Float probability;
        String endStateHeads;
        String endStateTails;
        HashMap<Boolean, String> outcome;
        Supplier<Boolean> coin;
        Pair<String, Integer> stateActionPair;
        while (machineResult.next()) {
            startState = machineResult.getString("start_state");
            action = machineResult.getInt("action");
            probability = machineResult.getFloat("probability");
            endStateHeads = machineResult.getString("end_state_heads");
            endStateTails = machineResult.getString("end_state_tails");
            assert 0 < probability && probability < 1;
            String finalEndStateTails = endStateTails;
            String finalEndStateHeads = endStateHeads;
            outcome = new HashMap<Boolean, String>() {{
                put(true, finalEndStateHeads);
                put(false, finalEndStateTails);
            }};
            //bernoulli
            coin = random.coinFlip(probability);

            stateActionPair = new Pair<String, Integer>(startState, action);
            HashMap<Boolean, String> finalOutcome = outcome;
            Supplier<Boolean> finalCoin = coin;
            transitionFunction.put(stateActionPair, () -> finalOutcome.get(finalCoin.get()));
        }

        return transitionFunction;
    }

    /**
     *
     * @param conn
     * @return A map associating states to rewards.
     * @throws SQLException
     */
    private Map<String, Float> readInRewards(Connection conn) throws SQLException {
        Statement statement;
        statement = conn.createStatement();
        ResultSet rewardsResult;
        rewardsResult = statement.executeQuery("SELECT state,reward FROM rewards;");

        HashMap<String, Float> rewards;
        rewards = new HashMap<String, Float>() {};

        while (rewardsResult.next()) {
            rewards.put(rewardsResult.getString("state"), rewardsResult.getFloat("reward"));
        }

        return rewards;
   }

    @Override
    public String toString() {
        double sum;
        sum = 0.0;
        for (Float reward: this.rewards.values()) {
            sum += reward;
        }
        String outp;
        outp = String.format("States: %s", String.valueOf(this.rewards.keySet().size()));
        outp += String.format("\tMoney on the table: %s", String.valueOf(sum));

        return outp;
    }


}
