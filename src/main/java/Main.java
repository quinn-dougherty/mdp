import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws IOException, SQLException {
        MDP mdp;
        Data data;

        data = new Data();
        //data.createData(32, 4);
        mdp = new MDP(data.connect());
        System.out.println(mdp.toString());
    }

}

