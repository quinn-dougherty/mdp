import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.List;

public class Agent {
    public String state;
    public double money;
    private MDP world;

    public Agent(MDP world, String startState) {
        assert world.rewards.keySet().contains(startState) : "startState is not in this MDP";
        this.state = startState;
        this.money = 0;
        this.world = world;
    }

    @Override
    public String toString() {
        return "In state " + this.state + " with $" + String.valueOf(this.money);
    }

    /**
     * take a single step, modifying state, and receiving reward.
     * @param action
     */
    public void step(Integer action) {
        Pair<String, Integer> stateActionPair;
        stateActionPair = new Pair<String, Integer>(this.state, action);
        this.state = this.world.transitionFunction.get(stateActionPair).get();
        this.money += this.world.rewards.get(this.state);
    }

    /**
     * Consume a list of actions / perform a walk.
     * @param actions
     * @return states visited along the walk.
     */
    public List<String> walk(List<Integer> actions) {
        ArrayList<String> visited;
        visited = new ArrayList<String>();
        visited.add(this.state);

        for (Integer action : actions) {
            this.step(action);
            visited.add(this.state);
        }
        return visited;
    }

}
