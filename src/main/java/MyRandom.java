import java.util.Random;
import java.util.function.Supplier;

public class MyRandom extends Random {

    public Supplier<Boolean> coinFlip(Float probabilityHeads) {
        /*Bernoulli variable*/
        if (!(probabilityHeads >= 0.0 && probabilityHeads <= 1.0))
            throw new IllegalArgumentException("probabilityHeads must be between 0.0 and 1.0: " + probabilityHeads);
        return () -> this.nextDouble() < probabilityHeads;
    }
}
